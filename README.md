# Meditation Tea Time Purpose

We come together as spiritual friends to sit in silence, feel held, and share
loving and respectful conversation. Our spiritual practice is to speak from the
heart and listen with respect and compassion. Participation is completely
voluntary. All that we say here remains confidential.

We share what is up for us in the moment -- that could be our response to
today's readings or something that is going on in our lives. With mindfulness,
we reflect on life and our condition in it. We are deeply committed to our
spiritual path.

Trusting and valuing our relationship, we listen and support rather than "fix"
each other or give unsolicited advice. We share our response to what has been
said by speaking for and about ourselves, not others. We do not interrupt or
interrogate others, and we intentionally pause after each comment to really
reflect on it and take it in. We intentionally create space for each person and
pay attention not to dominate the conversation. We treasure this place where we
are held with loving compassion and heard without intrusion or judgement.
